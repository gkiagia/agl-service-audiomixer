# Audio Mixer Service

Audio mixer binding service for AGL.

This binding exposes PipeWire mixer controls to applications.
